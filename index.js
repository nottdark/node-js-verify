const express = require('express'),
  cors = require('cors'),
  app = express(),
  port = process.env.PORT || 5000,
  axios = require('axios'),
  qs = require('qs')

// CORS
app.use(cors())

// Body parser
app.use(express.json())

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

const formatMessage = (formObj) => {
  let msg = ''
  msg = `\r\nTime: ${new Date().toLocaleString()} \r\n\r\n`
  for (const key in formObj) {
    if (formObj.hasOwnProperty(key)) {
      const element = formObj[key]
      msg += `${capitalize(key)}: ${element}\r\n`
    }
  }
  return msg
}

// Route
app.post('/send-noti', async (req, res) => {
  let headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    Authorization: `Bearer ${req.body.token}`,
  }
  if (!req.body.form) {
    return res.status(400).send({
      message: 'form feild is not empty.',
    })
  }
  if (typeof req.body.form != 'object' || req.body.form.constructor !== Object) {
    return res.status(400).send({
      message: 'form feild is not object.',
    })
  }
  try {
    const response = await axios.post(
      'https://notify-api.line.me/api/notify',
      qs.stringify({ message: formatMessage(req.body.form) }),
      { headers }
    )
    res.status(200).send(response)
  } catch (error) {
    res.status(400).send(error)
  }
})

app.listen(port, () => {
  console.log(`Server is running at port ${port}`)
})
